<?php

namespace Drupal\geofield_polygon_select;

/**
 * Interface FeatureCollectionStoreInterface.
 */
interface FeatureCollectionStoreInterface {

  /**
   * Gets a feature with specified key from collection.
   *
   * @param string $collection_id
   *   The collection id where the feature is looked up.
   * @param string $key
   *   The key that will be looked up.
   *
   * @returns string
   *   The feature that corresponds to the specified key in the collection.
   */
  public function getFeatureFromKey($collection_id, $key);

  /**
   * Gets the value of the key specified in keyholder value from a feature.
   *
   * @param string $collection_id
   *   The collection id where the feature is looked up.
   * @param string $feature
   *   The feature where will be the key looked up.
   *
   * @returns string
   *   The value of the key from keyholder in the feature.
   */
  public function getKeyFromFeature($collection_id, $feature);

}
